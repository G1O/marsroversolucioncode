# README #

Solucion al problema "Mars Rover"

Tecnologia usada:
### Language ###
   - Java 7

### Build and dependency management  ###
   - Maven 3.2.3 [(http://maven.apache.org/)](http://maven.apache.org/)

### Instrucciones ###

Una vez descargado el proyecto, este se debe importar desde el pom.xml desde cualquier IDE de desarrollo que soporte maven, posteriormente se deberá verificar la solución siguiente los siguientes pasos:

1. colocar dentro del archivo las instrucciones  (este se encuentra dentro del proyecto en la carpera "resources\instruccion.txt")
5, 5
1, 2, N
LMLMLMLMM
3, 3, E
MMRMMRMRRM
2. Dirigirse al archivo "ProcesarArchivo.java" y realizar un "Run" en el main(Todo esto dentro del IDE de desarrollo).