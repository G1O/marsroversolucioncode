/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package movimiento.procesar;

import movimiento.servicios.Archivo;
import movimiento.servicios.ProcesarMovimientos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: SerGIO
 * Date: 04-06-15
 * Time: 05:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProcesarArchivo {

    public static void main(String args[]) throws Exception {
        try{
            ProcesarMovimientos movimiento = new ProcesarMovimientos();
            Archivo a = new Archivo();
            List<String> listaInstrucciones = new ArrayList<String>();
            listaInstrucciones = a.leerArchivoTxt("instruccion.txt");
            for (String li : listaInstrucciones) {
                if(movimiento.esNumero(li.charAt(0)+"")){
                    movimiento.setMovimiento(li);
                }else{
                    movimiento.procesarAcciones(li);
                    movimiento.mostrarUbicacion();
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


}
