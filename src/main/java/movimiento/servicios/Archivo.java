package movimiento.servicios;
import java.io.*    ;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: SerGIO
 * Date: 04-06-15
 * Time: 05:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class Archivo {
    public List leerArchivoTxt(String url){
        List<String> listaInstruciones = new ArrayList<String>();
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource(url).getFile());
            BufferedReader bf= new BufferedReader(new FileReader(file));
            String bfLeer;
            while ((bfLeer = bf.readLine())!=null)  {
                listaInstruciones.add(bfLeer);
            }
        }catch (Exception e){
            System.out.println("error al leer el archivo");
            e.printStackTrace();
        }
        return listaInstruciones;
    }
}
