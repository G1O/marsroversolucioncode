/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package movimiento.servicios;

import movimiento.procesar.ProcesarArchivo;

/**
 * Created with IntelliJ IDEA.
 * User: SerGIO
 * Date: 04-06-15
 * Time: 05:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProcesarMovimientos {
        
    private Integer x = 0;//coordenadas x inicial
    private Integer y = 0;//coordenadas y inicial
    private Integer apuntaFrente = N;//apunta de frente posicion inicial
    public static final Integer N = 1; //norte
    public static final Integer E = 2; //este
    public static final Integer S = 3;//sur
    public static final Integer O = 4;//oeste
    
    public ProcesarMovimientos() {
    }
    public void setMovimiento(Integer x, Integer y, Integer apuntaFrente) {
        this.x = x;
        this.y = y;
        this.apuntaFrente = apuntaFrente;
    }
    public void setMovimiento(String cadenaMovimientos) throws Exception {
        Integer accion1=null;
        Integer accion2=null;
        Integer accion3=null;
        for (int i = 0; i < cadenaMovimientos.length(); i++  ) {
            if((cadenaMovimientos.charAt(i)!=' ') && (cadenaMovimientos.charAt(i)!=',') && (esNumero(cadenaMovimientos.charAt(i) + "")|| cadenaMovimientos.charAt(i)=='N'||cadenaMovimientos.charAt(i)=='S'||cadenaMovimientos.charAt(i)=='E'||cadenaMovimientos.charAt(i)=='O')){
                if (accion1==null && esNumero(cadenaMovimientos.charAt(i) + "")){
                    accion1 = Integer.parseInt(cadenaMovimientos.charAt(i)+"");
                }else if (accion2==null && esNumero(cadenaMovimientos.charAt(i) + "")){
                    accion2 = Integer.parseInt(cadenaMovimientos.charAt(i)+"");
                }else if (accion3==null){
                    accion3 = movimiento(cadenaMovimientos.charAt(i));
                }else {
                    throw new Exception("Error en la cantidad de parametros");
                }
            }
        }
        if (accion1!=null && accion2!=null && accion3!=null){
            this.setMovimiento(accion1, accion2, accion3);
        }else if(accion1!=null && accion2!=null){
            this.setMovimiento(accion1, accion2);
        }else {
            throw new Exception("Error en formato de los parametros");
        }
    }
    public void setMovimiento(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }
    public void mostrarUbicacion() {
        char puntoCardinal = 'N';// mirando al norte por defecto
        switch(apuntaFrente){
            case 1:
               puntoCardinal = 'N';
               break;
            case 2:
               puntoCardinal = 'E';
               break;
            case 3:
               puntoCardinal = 'S';
               break;
            case 4:
               puntoCardinal = 'O';  
               break;
        }
        System.out.println(x+" "+y+" "+puntoCardinal);
    }
    public void procesarAcciones(String cadenaAcciones) throws Exception {
        for (int i = 0; i < cadenaAcciones.length(); i++  ) {
            accion(cadenaAcciones.charAt(i));
        }
    }
    private Integer movimiento(Character movimiento) throws Exception {
        Integer puntoCardinal=null;
        switch(movimiento){
            case 'N':
                puntoCardinal = N;
                break;
            case 'O':
                puntoCardinal = O;
                break;
            case 'S':
                puntoCardinal = S;
                break;
            case 'E':
                puntoCardinal = E;
                break;
            default:
                throw new Exception("El punto cardinal '"+movimiento+"' no esta existe");
        }
        return puntoCardinal;
    }
    private void accion(Character accion) throws Exception {
        switch(accion){
            case 'L':
               girarIzquierda();
               break;
            case 'R':
               girarDerecha();
               break;
            case 'M':
               avanzar();
               break;
            default:
               throw new Exception("El movimiento '"+accion+"' no esta permitdo");
        }
    }
    private void avanzar() {
        switch(apuntaFrente){
            case 1:
               this.y++;
               break;
            case 2:
               this.x++;
               break;
            case 3:
               this.y--;
               break;
            case 4:
               this.x--;  
               break;
        }
    }
    private void girarIzquierda() {
        apuntaFrente = (apuntaFrente - 1) < N ? O : apuntaFrente - 1;
    }
    private void girarDerecha() {
        apuntaFrente = (apuntaFrente +  1) > O ? N: apuntaFrente + 1;
    }
    public boolean esNumero(String s){
        return s.toLowerCase() == s.toUpperCase();
    }
}
